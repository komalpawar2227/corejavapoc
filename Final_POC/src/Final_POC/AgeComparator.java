package Final_POC;

import java.util.Comparator;

public class AgeComparator implements Comparator {
    public int compare(Object o5, Object o6) {
    	UserDemo s5 = (UserDemo) o5;
    	UserDemo s6 = (UserDemo) o6;

 

        return s5.Age.compareTo(s6.Age);
    }
}
